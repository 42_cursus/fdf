/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf_error.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <adelhom@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/23 21:24:11 by adelhom           #+#    #+#             */
/*   Updated: 2017/06/08 12:16:18 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"
#include "libft.h"

void	map_error(char *code)
{
	ft_putstr_fd("map error : ", 2);
	ft_putendl_fd(code, 2);
	exit(1);
}

void	data_error(char *code)
{
	ft_putstr_fd("data error : ", 2);
	ft_putendl_fd(code, 2);
	exit(1);
}

void	malloc_error(char *code)
{
	ft_putstr_fd("malloc error : ", 2);
	ft_putendl_fd(code, 2);
	exit(1);
}

void	argument_error(char *code)
{
	ft_putstr_fd("argument error : ", 2);
	ft_putendl_fd(code, 2);
	exit(1);
}
