/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <adelhom@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/16 15:18:07 by adelhom           #+#    #+#             */
/*   Updated: 2017/06/08 12:12:34 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "mlx.h"
#include "fdf.h"

static void	check_args(int argc, char **argv)
{
	char	*extension;

	if (argc != 2)
		argument_error(USAGE_ERROR);
	if (ft_strlen(argv[1]) > 4)
		if ((extension = ft_strchr(argv[1], '.')))
			if (ft_strcmp(extension, ".fdf") == 0)
				return ;
	argument_error(FILE_EXTENSION_ERROR);
}

int			main(int argc, char **argv)
{
	t_data	*data;
	t_map	*map;

	check_args(argc, argv);
	map = parse_map(argv[1]);
	data = init_data(map);
	test_show_map(map);
	mlx_hook(data->win, 2, 3, input, data);
	draw(data);
	print(data);
	mlx_loop(data->mlx);
	return (0);
}
