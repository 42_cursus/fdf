/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_point2dnew.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <adelhom@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/03 15:41:48 by adelhom           #+#    #+#             */
/*   Updated: 2017/02/22 15:55:12 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_point2d	*ft_point2dnew(double x, double y)
{
	t_point2d		*point;

	point = ft_memalloc(sizeof(t_point2d));
	point->x = x;
	point->y = y;
	return (point);
}
