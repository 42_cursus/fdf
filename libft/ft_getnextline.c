/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_getnextline.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <adelhom@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/16 11:52:33 by adelhom           #+#    #+#             */
/*   Updated: 2017/02/14 11:22:34 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static char		*extract_rest(t_file *file)
{
	int	i;

	i = 0;
	file->endl = FALSE;
	while (file->index + i < file->size)
	{
		if (file->buf[file->index + i] == ENDL)
		{
			file->endl = TRUE;
			i++;
			break ;
		}
		i++;
	}
	file->index += i;
	return (ft_strsub(file->buf, file->index - i, i - file->endl));
}

static t_file	*get_file(t_list **lst, int fd)
{
	t_file	*file;
	t_list	*tmp;

	tmp = *lst;
	while (tmp)
	{
		file = (t_file *)(tmp->content);
		if (file->fd == fd)
			return (file);
		tmp = tmp->next;
	}
	file = (t_file *)ft_memalloc(sizeof(t_file));
	file->buf = ft_strnew(BUFF_SIZE);
	file->size = BUFF_SIZE;
	file->index = BUFF_SIZE;
	file->fd = fd;
	file->endl = TRUE;
	tmp = ft_lstnew(file, sizeof(t_file));
	ft_memdel((void **)&file);
	ft_lstadd(lst, tmp);
	return ((t_file *)(tmp->content));
}

static void		remove_file(t_list **lst, int fd, char **buffer)
{
	t_file	*file;
	t_list	**tmp;
	t_list	*ptr;

	tmp = lst;
	while (*tmp)
	{
		file = (t_file *)((*tmp)->content);
		if (file->fd == fd)
			break ;
		*tmp = ((*tmp)->next);
	}
	if (*tmp)
	{
		ptr = (*tmp)->next;
		ft_strdel(&(file->buf));
		ft_memdel((void **)&file);
		ft_memdel((void **)tmp);
		*tmp = ptr;
	}
	ft_strdel(buffer);
}

static int		read_buffer(t_file *file, t_list **lst, char **buffer, char **l)
{
	if (file->index == file->size)
	{
		file->size = read(file->fd, file->buf, BUFF_SIZE);
		if (file->size == -1)
		{
			remove_file(lst, file->fd, buffer);
			return (-1);
		}
		file->index = 0;
		if (file->size == 0)
		{
			if (!file->endl)
			{
				*l = *buffer;
				return (1);
			}
		}
	}
	return (0);
}

int				ft_getnextline(int const fd, char **line)
{
	static t_list	*lst;
	t_file			*file;
	char			*buffer;
	int				ret;

	if (fd < 0 || !line)
		return (-1);
	file = get_file(&lst, fd);
	buffer = NULL;
	while (file->size > 0)
	{
		if ((ret = read_buffer(file, &lst, &buffer, line)) != 0)
			return (ret);
		while (file->index < file->size)
		{
			buffer = ft_strmerge(buffer, extract_rest(file));
			if (file->endl)
			{
				*line = buffer;
				return (1);
			}
		}
	}
	remove_file(&lst, fd, &buffer);
	return (0);
}
