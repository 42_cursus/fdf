# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: adelhom <adelhom@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/01/16 12:46:43 by adelhom           #+#    #+#              #
#    Updated: 2017/03/01 16:32:04 by adelhom          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME	= fdf

# src files
SRC		= fdf.c 		\
		  fdf_parse.c	\
		  fdf_convert.c \
		  fdf_input.c	\
		  fdf_utils.c	\
		  fdf_draw.c	\
		  fdf_init.c	\
		  fdf_error.c	\
		  fdf_test.c	\
		  fdf_print.c	\

# obj files
OBJ = $(SRC:.c=.o)

# compiler
CC		= gcc
CFLAGS	= -Wall -Wextra -Werror -g

# mlx library
MLX		= ./minilibx/
MLX_LIB	= $(addprefix $(MLX),mlx.a)
MLX_INC	= -I ./minilibx
MLX_LNK	= -L ./minilibx -l mlx -framework OpenGL -framework AppKit

# LIBFT library
LIBFT		= ./libft/
LIBFT_LIB	= $(addprefix $(LIBFT),libft.a)
LIBFT_INC	= -I ./libft/includes
LIBFT_LNK	= -L ./libft -l ft

all: $(LIBFT_LIB) $(MLX_LIB) $(NAME)

%.o: %.c
	$(CC) $(CFLAGS) $(MLX_INC) $(LIBFT_INC) -o $@ -c $<

$(LIBFT_LIB):
	make -C $(LIBFT)

$(MLX_LIB):
	make -C $(MLX)

$(NAME): $(OBJ)
	$(CC) $(OBJ) $(MLX_LNK) $(LIBFT_LNK) -lm -o $(NAME)

clean:
	rm -rf $(OBJ)
	make -C $(LIBFT) clean
	make -C $(MLX) clean

fclean: clean
	rm -rf $(NAME)
	make -C $(LIBFT) fclean

re: fclean all
