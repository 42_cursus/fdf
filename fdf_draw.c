/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf_draw.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <adelhom@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/04 16:38:45 by adelhom           #+#    #+#             */
/*   Updated: 2017/06/08 12:08:41 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"
#include "mlx.h"

static void		draw_pixel(t_point2d *pt, t_data *d, t_rgba *rgba)
{
	unsigned int	c;

	c = convert_rgba(rgba);
	mlx_pixel_put(d->mlx, d->win, pt->x + HEIGHT / 2, pt->y + WIDTH / 2, c);
}

static void		drawline_point(int s, t_point2d *p1, t_point2d *p2, t_data *d)
{
	int			i;
	double		xi;
	double		yi;
	t_point2d	*tmp;

	tmp = ft_point2dnew(p1->x, p1->y);
	xi = (double)(p2->x - p1->x) / s;
	yi = (double)(p2->y - p1->y) / s;
	i = -1;
	while (++i <= s)
	{
		draw_pixel(ft_point2dnew(tmp->x, tmp->y), d, d->color);
		tmp->x = p1->x + i * xi;
		tmp->y = p1->y + i * yi;
	}
	free(tmp);
}

static void		draw_line(t_pixel *p1, t_pixel *p2, t_data *d)
{
	int			step;

	if (pixel_valide(p1->pt2d->x, p1->pt2d->y) == 0 &&
	pixel_valide(p2->pt2d->x, p2->pt2d->y) == 0)
		return ;
	if (fabs(p1->pt2d->x - p2->pt2d->x) <= fabs(p1->pt2d->y - p2->pt2d->y))
		step = 1 + fabs(p1->pt2d->y - p2->pt2d->y);
	else
		step = 1 + fabs(p1->pt2d->x - p2->pt2d->x);
	set_color(d, p1->pt3d->z, p2->pt3d->z);
	drawline_point(step, p1->pt2d, p2->pt2d, d);
}

void			draw(t_data *d)
{
	t_pixel		*p1;
	t_point2d	p;

	convert_pixel(d);
	p.y = 0;
	while (p.y < d->map->length)
	{
		p.x = 0;
		while (p.x < d->map->lines[(int)p.y]->length)
		{
			p1 = d->map->lines[(int)p.y]->pixel[(int)p.x];
			if (p.x + 1 < d->map->lines[(int)p.y]->length)
				draw_line(p1, d->map->lines[(int)p.y]->pixel[(int)p.x + 1], d);
			if (p.y + 1 < d->map->length)
				if (p.x < d->map->lines[(int)p.y + 1]->length)
					draw_line(p1, d->map->lines[(int)p.y + 1]->pixel[(int)p.x],
					d);
			p.x++;
		}
		p.y++;
	}
}
