/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf_parse.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <adelhom@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/19 15:15:07 by adelhom           #+#    #+#             */
/*   Updated: 2017/06/08 12:11:05 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static void		set_max_value(t_map *map, int x, int y, int z)
{
	if (x > map->max_x)
		map->max_x = x;
	if (y > map->max_y)
		map->max_y = y;
	if (z > map->max_z)
		map->max_z = z;
}

static t_line	*build_line(t_map *map, char *str, int y)
{
	t_line	*line;
	char	**split;
	int		x;
	int		z;

	if (!(line = (t_line*)malloc(sizeof(t_line))))
		malloc_error("build_line (line)");
	x = ft_splitcount(str, ' ');
	if (!(line->pixel = (t_pixel **)malloc(x * sizeof(t_pixel *))))
		malloc_error("build_line (line->pixel)");
	split = ft_strsplit(str, ' ');
	line->length = x;
	while (x-- != 0)
	{
		if (!(line->pixel[x] = (t_pixel *)malloc(sizeof(t_pixel))))
			malloc_error("build_line (line->pixel[])");
		z = ft_getnbr(split[x]);
		line->pixel[x]->pt3d = ft_point3dnew(x, y, z);
		line->pixel[x]->pt2d = ft_point2dnew(0, 0);
		set_max_value(map, x, y, z);
	}
	return (line);
}

static t_map	*build_map(int nb_lines)
{
	t_map	*map;

	if (!(map = (t_map*)malloc(sizeof(t_map))))
		malloc_error("build_map (map)");
	if (!(map->lines = (t_line**)malloc(nb_lines * sizeof(t_line*))))
		malloc_error("build_map (map->line)");
	map->length = 0;
	map->max_z = 0;
	map->max_y = 0;
	map->max_x = 0;
	return (map);
}

static int		get_number_line(char *mapfile)
{
	int		fd;
	int		nb_lines;
	char	buffer;

	fd = 0;
	nb_lines = 0;
	if ((fd = open(mapfile, O_RDONLY)) < 0)
		map_error(FILE_EXIST_ERROR);
	while (read(fd, &buffer, 1))
		if (buffer == '\n')
			nb_lines++;
	close(fd);
	return (nb_lines);
}

t_map			*parse_map(char *mapfile)
{
	t_map	*map;
	t_line	*line_map;
	char	*line;
	int		fd;
	int		nb_line;

	map = build_map(get_number_line(mapfile));
	nb_line = 0;
	if ((fd = open(mapfile, O_RDONLY)) < 0)
		map_error(FILE_EXIST_ERROR);
	while ((ft_getnextline(fd, &line)) > 0)
	{
		line_map = build_line(map, line, nb_line);
		map->lines[nb_line] = line_map;
		nb_line++;
	}
	if (nb_line == 0)
		map_error("size 0");
	map->length = nb_line;
	return (map);
}
