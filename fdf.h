/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <adelhom@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/19 15:00:19 by adelhom           #+#    #+#             */
/*   Updated: 2017/06/08 12:07:48 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H

# include "libft.h"
# include <math.h>

# define USAGE_ERROR "usage : ./fdf <filename>"
# define FILE_EXTENSION_ERROR "bad extension file"
# define FILE_FORMAT_ERROR "bad format file"
# define FILE_EXIST_ERROR "file doesn't exist"

# define WIDTH 1600
# define HEIGHT 600

# define KEY_ARROW_UP		126
# define KEY_ARROW_RIGHT	124
# define KEY_ARROW_DOWN		125
# define KEY_ARROW_LEFT		123
# define KEY_ARROW_Z		13
# define KEY_ARROW_D		2
# define KEY_ARROW_S		1
# define KEY_ARROW_Q		0
# define KEY_ESC			53
# define KEY_ZOOM_IN	88
# define KEY_ZOOM_OUT	85
# define KEY_UP_Z		87
# define KEY_DOWN_Z		84

typedef struct	s_rgba
{
	int	r;
	int	g;
	int	b;
	int	a;
}				t_rgba;

typedef struct	s_pixel
{
	t_point3d	*pt3d;
	t_point2d	*pt2d;
}				t_pixel;

typedef struct	s_line
{
	t_pixel		**pixel;
	int			length;
}				t_line;

typedef struct	s_map
{
	t_line	**lines;
	int		length;
	int		max_z;
	int		max_y;
	int		max_x;
}				t_map;

typedef struct	s_view
{
	double			x;
	double			y;
	double			z;
	double			rx;
	double			ry;
	double			rz;
	double			zoom;
}				t_view;

typedef struct	s_data
{
	void	*mlx;
	void	*win;
	t_map	*map;
	t_view	*view;
	t_rgba	*color;
	int		width;
	int		height;
	int		space;
}				t_data;

void			print(t_data *d);

void			test_show_map(t_map *map);

void			draw(t_data *d);

t_data			*init_data(t_map *map);
t_rgba			*init_rgba(int r, int g, int b, int a);

void			convert_pixel(t_data *d);
unsigned int	convert_rgba(t_rgba *rgba);

t_bool			pixel_valide(int x, int y);
void			set_color(t_data *d, double z1, double z2);
void			move_z(t_data *d, int value);

int				input(int keycode, t_data *d);

t_map			*parse_map(char *mapfile);

void			map_error(char *code);
void			malloc_error(char *code);
void			argument_error(char *code);

#endif
