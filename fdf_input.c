/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf_input.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <adelhom@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/04 14:01:51 by adelhom           #+#    #+#             */
/*   Updated: 2017/06/08 12:09:52 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"
#include "mlx.h"

static void		input_move(int keycode, t_data *data)
{
	if (keycode == KEY_ARROW_UP)
		data->view->y += data->map->max_x;
	if (keycode == KEY_ARROW_DOWN)
		data->view->y -= data->map->max_x;
	if (keycode == KEY_ARROW_RIGHT)
		data->view->x -= data->map->max_x;
	if (keycode == KEY_ARROW_LEFT)
		data->view->x += data->map->max_x;
	if (keycode == KEY_ARROW_Z)
		data->view->z += data->map->max_x;
	if (keycode == KEY_ARROW_S)
		data->view->z -= data->map->max_x;
}

static	void	input_zoom(int keycode, t_data *data)
{
	if (keycode == KEY_ZOOM_IN)
		data->view->zoom += 10;
	else if (keycode == KEY_ZOOM_OUT)
		data->view->zoom -= 10;
}

static	void	input_change_space(int keycode, t_data *data)
{
	if (keycode == KEY_UP_Z && data->space < 25)
		data->space += 1;
	else if (keycode == KEY_DOWN_Z && data->space >= 3)
		data->space -= 1;
}

int				input(int keycode, t_data *data)
{
	if (keycode == KEY_ESC)
	{
		mlx_destroy_window(data->mlx, data->win);
		exit(0);
	}
	input_move(keycode, data);
	input_zoom(keycode, data);
	input_change_space(keycode, data);
	mlx_clear_window(data->mlx, data->win);
	print(data);
	draw(data);
	return (0);
}
