/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf_init.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <adelhom@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/13 15:49:12 by adelhom           #+#    #+#             */
/*   Updated: 2017/06/08 12:09:34 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"
#include "mlx.h"

t_view	*init_view(t_map *map)
{
	t_view	*v;

	if (!(v = (t_view *)malloc(sizeof(t_view))))
		malloc_error("init_view view");
	v->x = map->max_x * 6;
	v->y = map->max_y * 13;
	v->z = -8 * map->max_x;
	v->rx = M_PI / 6;
	v->ry = 0;
	v->rz = -M_PI / 6;
	v->zoom = 1000;
	return (v);
}

t_data	*init_data(t_map *map)
{
	t_data	*data;

	if (!(data = (t_data *)malloc(sizeof(t_data))))
		malloc_error("init_data data");
	data->view = init_view(map);
	data->mlx = mlx_init();
	data->width = WIDTH;
	data->height = HEIGHT;
	data->map = map;
	data->win = mlx_new_window(data->mlx, data->width, data->height, "FDF");
	data->space = 10;
	return (data);
}

t_rgba	*init_rgba(int r, int g, int b, int a)
{
	t_rgba *rgba;

	if (!(rgba = (t_rgba *)malloc(sizeof(t_rgba))))
		malloc_error("RGBA");
	rgba->r = r;
	rgba->g = g;
	rgba->b = b;
	rgba->a = a;
	return (rgba);
}
