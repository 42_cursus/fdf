/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf_utils.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <adelhom@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/25 17:59:37 by adelhom           #+#    #+#             */
/*   Updated: 2017/06/08 12:13:33 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

t_bool	pixel_valide(int x, int y)
{
	if ((x >= WIDTH / 2 || x <= WIDTH / 2)
	&& (y >= HEIGHT / 2 || y <= HEIGHT / 2))
		return (TRUE);
	return (FALSE);
}

void	set_color(t_data *d, double z1, double z2)
{
	int c1;
	int c2;

	c1 = (z1 > z2) ? z1 : z2;
	c2 = (z1 > z2) ? z2 : z1;
	d->color = init_rgba(60 + c1 + c2, 3 * c2, 10 * c1, 1);
}
