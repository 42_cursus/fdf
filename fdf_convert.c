/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf_convert.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <adelhom@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/14 11:21:52 by adelhom           #+#    #+#             */
/*   Updated: 2017/06/08 12:03:10 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static void		convert_one_pixel(t_pixel *pix, t_data *d)
{
	t_point3d	c;
	t_point3d	*tmp;

	tmp = ft_point3dnew(0, 0, 0);
	c.x = (pix->pt3d->x * d->space) - d->view->x;
	c.y = (pix->pt3d->y * d->space) - d->view->y;
	c.z = (-pix->pt3d->z - d->view->z);
	tmp->x = (cos(d->view->ry) * ((sin(d->view->rz) * c.y) +
	(cos(d->view->rz) * c.x))) - (sin(d->view->ry) * c.z);
	tmp->y = (sin(d->view->rx) * ((cos(d->view->ry) * c.z) + (sin(d->view->ry) *
	((sin(d->view->ry) * c.y) + (cos(d->view->rz) * c.x))))) +
	(cos(d->view->rx) * ((cos(d->view->ry) * c.y) - (sin(d->view->rz) * c.x)));
	tmp->z = (cos(d->view->rx) * ((cos(d->view->ry) * c.z) + (sin(d->view->ry) *
	((sin(d->view->ry) * c.y) + (cos(d->view->rz) * c.x))))) -
	(sin(d->view->rx) * ((cos(d->view->rz) * c.y) - (sin(d->view->rz) * c.x)));
	pix->pt2d->x = (d->view->zoom / tmp->z) * tmp->x;
	pix->pt2d->y = (d->view->zoom / tmp->z) * tmp->y;
	free(tmp);
}

void			convert_pixel(t_data *d)
{
	t_point2d	p;

	p.y = 0;
	while (p.y < d->map->length)
	{
		p.x = 0;
		while (p.x < d->map->lines[(int)p.y]->length)
		{
			convert_one_pixel(d->map->lines[(int)p.y]->pixel[(int)p.x], d);
			p.x++;
		}
		p.y++;
	}
}

unsigned int	convert_rgba(t_rgba *rgba)
{
	return ((rgba->a << 24) + (rgba->r << 16) + (rgba->g << 8) + rgba->b);
}
