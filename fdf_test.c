/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf_test.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <adelhom@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/25 14:11:46 by adelhom           #+#    #+#             */
/*   Updated: 2017/06/08 12:13:01 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "mlx.h"
#include "fdf.h"
#include "libft.h"

static void	test_show_max(t_map *map)
{
	ft_putstr("\nMAP LENGTH : \x1B[32m");
	ft_putnbr(map->length);
	ft_putstr("\x1B[0m\nMAX X : \x1B[32m");
	ft_putnbr(map->max_x);
	ft_putstr("\x1B[0m\nMAX Y : \x1B[32m");
	ft_putnbr(map->max_y);
	ft_putstr("\x1B[0m\nMAX Z : \x1B[32m");
	ft_putnbr(map->max_z);
	ft_putendl("\x1B[0m");
}

void		test_show_map(t_map *map)
{
	t_point2d pt;

	pt.y = 0;
	while (pt.y < map->length)
	{
		ft_putstr("\nLINE \x1B[34m");
		ft_putnbr(pt.y);
		ft_putstr("\x1B[0m | length : \x1B[34m");
		ft_putnbr(map->lines[(int)pt.y]->length);
		ft_putstr("\x1B[0m");
		pt.x = 0;
		while (pt.x < map->lines[(int)pt.y]->length)
		{
			ft_putstr("\n(\x1B[35m");
			ft_putnbr(map->lines[(int)pt.y]->pixel[(int)pt.x]->pt3d->x);
			ft_putstr("\x1B[0m, \x1B[35m");
			ft_putnbr(map->lines[(int)pt.y]->pixel[(int)pt.x]->pt3d->y);
			ft_putstr("\x1B[0m, \x1B[35m");
			ft_putnbr(map->lines[(int)pt.y]->pixel[(int)pt.x]->pt3d->z);
			ft_putstr("\x1B[0m)");
			pt.x++;
		}
		pt.y++;
	}
	test_show_max(map);
}
