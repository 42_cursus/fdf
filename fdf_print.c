/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf_print.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <adelhom@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/28 13:59:36 by adelhom           #+#    #+#             */
/*   Updated: 2017/06/08 12:11:37 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"
#include "mlx.h"

static void	print_view_data(t_data *d)
{
	mlx_string_put(d->mlx, d->win, 10, 10, 0x00FFFFFF,
	"========= VIEW INFOS =========");
	mlx_string_put(d->mlx, d->win, 100, 42, 0x00FFFFFF, "ZOOM :");
	mlx_string_put(d->mlx, d->win, 160, 42, 0x00FFFFFF, ft_itoa(d->view->zoom));
	mlx_string_put(d->mlx, d->win, 250, 42, 0x00FFFFFF, "  X :");
	mlx_string_put(d->mlx, d->win, 300, 42, 0x00FFFFFF, ft_itoa(d->view->x));
	mlx_string_put(d->mlx, d->win, 400, 42, 0x00FFFFFF, "  Y :");
	mlx_string_put(d->mlx, d->win, 450, 42, 0x00FFFFFF, ft_itoa(d->view->y));
	mlx_string_put(d->mlx, d->win, 550, 42, 0x00FFFFFF, "  Z :");
	mlx_string_put(d->mlx, d->win, 600, 42, 0x00FFFFFF, ft_itoa(d->view->z));
	mlx_string_put(d->mlx, d->win, 700, 42, 0x00FFFFFF, "SPACE :");
	mlx_string_put(d->mlx, d->win, 770, 42, 0x00FFFFFF, ft_itoa(d->space));
}

static void	print_max_value_data(t_data *d)
{
	mlx_string_put(d->mlx, d->win, 10, 72, 0x00FFFFFF,
	"========= MAX VALUES INFOS =========");
	mlx_string_put(d->mlx, d->win, 100, 92, 0x00FFFFFF, "X   :");
	mlx_string_put(d->mlx, d->win, 150, 92, 0x00FFFFFF, ft_itoa(d->map->max_x));
	mlx_string_put(d->mlx, d->win, 250, 92, 0x00FFFFFF, "Y   :");
	mlx_string_put(d->mlx, d->win, 300, 92, 0x00FFFFFF, ft_itoa(d->map->max_y));
	mlx_string_put(d->mlx, d->win, 400, 92, 0x00FFFFFF, "Z   :");
	mlx_string_put(d->mlx, d->win, 450, 92, 0x00FFFFFF, ft_itoa(d->map->max_z));
}

static void	print_command_help(t_data *d)
{
	mlx_string_put(d->mlx, d->win, WIDTH - 280, 10, 0x00FFFFFF,
	"========= COMMAND =========");
	mlx_string_put(d->mlx, d->win, WIDTH - 280, 35, 0x00FFFFFF, "ZOOM     :");
	mlx_string_put(d->mlx, d->win, WIDTH - 170, 35, 0x00FFFFFF, "PAV6 ; PAV3");
	mlx_string_put(d->mlx, d->win, WIDTH - 280, 60, 0x00FFFFFF, "SPACE    :");
	mlx_string_put(d->mlx, d->win, WIDTH - 170, 60, 0x00FFFFFF, "PAV5 ; PAV2");
	mlx_string_put(d->mlx, d->win, WIDTH - 280, 85, 0x00FFFFFF, "MOVE X   :");
	mlx_string_put(d->mlx, d->win, WIDTH - 170, 85, 0x00FFFFFF, "RIGHT ; LEFT");
	mlx_string_put(d->mlx, d->win, WIDTH - 280, 110, 0x00FFFFFF, "MOVE Y   :");
	mlx_string_put(d->mlx, d->win, WIDTH - 170, 110, 0x00FFFFFF, "UP ; DOWN");
	mlx_string_put(d->mlx, d->win, WIDTH - 280, 135, 0x00FFFFFF, "MOVE Z   :");
	mlx_string_put(d->mlx, d->win, WIDTH - 170, 135, 0x00FFFFFF, "Z ; Q");
}

void		print(t_data *d)
{
	print_view_data(d);
	print_max_value_data(d);
	print_command_help(d);
}
